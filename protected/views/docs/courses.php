<?php if($this->beginCache('courses')) { ?>
<?php $this->beginWidget('CMarkdown');?>

# Документація по API курсів в НаУКМА

---


## Предмети і кредити

`<?php echo $this->createAbsoluteUrl('/api/v1/courses'); ?>/subjects`

### Параметри:

- program - назва програми
- year - рік навчання
- term - 1-осінній, 2-весняний, 3-літній
- level - Магістр, Бакалавр, Спеціаліст
- type - 'норм' - нормативні, 'вибір' - вибіркові

`<?php echo $this->createAbsoluteUrl('/api/v1/courses'); ?>/subjects?program=Фізика&year=1&term=1&level=Бакалавр&type=норм`


    {
        "Українська мова":"1.5",
        "Фізичне виховання":"0",
        "Англійська мова-1":"2",
        "Українська мова (для іноземних студентів)":"3",
        "Основи матаналізу":"4",
        "Методи та засоби комп`ютерних технологій":"2.5",
        "Мови програмування":"6",
        "Основи дискретної математики":"6"
    }


---

## Факультети

`<?php echo $this->createAbsoluteUrl('/api/v1/courses'); ?>/faculties`
### Параметри:

- field - faculty_full, faculty_short, faculty_en

`<?php echo $this->createAbsoluteUrl('/api/v1/courses'); ?>/faculties?field=faculty_full`

    [
        "Факультет інтформатики",
        "Факультет гуманітарних наук",
        "Факультет економічних наук",
        "Факультет правничих наук",
        "Факультет природничих наук",
        "Факультет соціальних наук і соціальних технологій"
    ]

---
## Програми

`<?php echo $this->createAbsoluteUrl('/api/v1/courses'); ?>/programs`
### Параметри:

- faculty - факультет скорочено, або для всіх факультетів - all.
- level - Бакалавр, Спеціаліст, Магістр
- field - faculty_full, faculty_short, faculty_eng

`<?php echo $this->createAbsoluteUrl('/api/v1/courses'); ?>/programs?faculty=ФІ&level=Бакалавр`

    [
        "Прикладна математика",
        "Програмна інженерія"
    ]

`<?php echo $this->createAbsoluteUrl('/api/v1/courses'); ?>/programs?faculty=all&level=Бакалавр`

    {
    "ФГН": [
        "Історія",
        "Культурологія",
        "Філологія",
        "Філософія"
    ],
    "ФПрН": [
        "Біологія",
        "Екологія, охорона навколишнього середовища та збалансоване природокористування",
        "Фізика",
        "Хімія"
    ],
    ...
    }

---

## Відсортовані по роках предмети

`<?php echo $this->createAbsoluteUrl('/api/v1/courses'); ?>/orderedSubjects`
### Параметри:

Застосовувати або faculty або program. Не застосовувати разом!!!

- faculty - факультет скорочено
- program - назва програми

`<?php echo $this->createAbsoluteUrl('/api/v1/courses'); ?>/orderedSubjects?program=Програмна інженерія`


    [
        {
        "level":"Бакалавр",
        "code":"6.050103",
        "program":"Програмна інженерія",
        "course_code":"91394",
        "course":"Українська мова",
        "type":"Нормативні навчальні дисципліни та практика",
        "credits":"1.5",
        "year":"1",
        "term":"Осінній семестр",
        "description":"",
        "faculty_short":"ФІ",
        "faculty_full":"Факультет інтформатики",
        "faculty_en":"FI"
        },
        ...
    ]

---

## Автокомліт предметів

`<?php echo $this->createAbsoluteUrl('/api/v1/courses'); ?>/autocomplete`
### Параметри:

- q - пошукова фраза
- limit - ліміт (по замовчуванню 10)
- offset - зсув (по замовчуванню 0)
- match - повний збіг (по замовчуванню 0)

`<?php echo $this->createAbsoluteUrl('/api/v1/courses'); ?>/autocomplete?q=а`

    [
        {
        "id": "\"Вік Гете\" в німецькій літературі",
        "course": "\"Вік Гете\" в німецькій літературі",
        "programs": [
            "Філологія"
        ],
        "faculties": [
            "ФГН"
        ],
        "term": "Весняний семестр",
        "year": "2",
        "level": "Бакалавр",
        "credits": "2"
        },
        ...
    ]
<?php $this->endWidget();?>
<?php $this->endCache(); } ?>


