<?php

/**
 * This is the model class for table "courses".
 *
 * The followings are the available columns in table 'courses':
 *
 * @property string $level
 * @property string $code
 * @property string $program
 * @property string $course_code
 * @property string $course
 * @property string $type
 * @property double $credits
 * @property double $year
 * @property string $term
 * @property string $description
 */
class Courses extends CActiveRecord
{
    public $level;
    public $code;
    public $program;
    public $course_code;
    public $type;
    public $course;
    public $year;
    public $term;
    public $credits;
    public $description;

    const
        TERM_AUTUMN = 'Осінній семестр',
        TERM_SPRING = 'Весняний семестр',
        TERM_SUMMER = 'Додатковий період весняного семестру',
        TYPE_REGULAR = 'Нормативні навчальні дисципліни та практика',
        TYPE_FREE = 'Вибіркові навчальні дисципліни',
        LEVEL_BACHELOR = 'Бакалавр',
        LEVEL_MASTER = 'Магістр',
        LEVEL_SPECIALIST = 'Спеціаліст';

    /**
     * Returns the static model of the specified AR class.
     *
     * @param string $className active record class name.
     *
     * @return Courses the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'courses';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        return array(
            array(
                'level, code, program, course_code, course, type, credits, year, term, description', 'safe',
                'on' => 'render'
            ),
        );
    }

    public function attributeLabels()
    {
        return array(
            'level' => 'Level',
            'code' => 'Code',
            'program' => 'Program',
            'course_code' => 'Course Code',
            'course' => 'Course',
            'type' => 'Type',
            'credits' => 'Credits',
            'year' => 'Year',
            'term' => 'Term',
            'description' => 'Description',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search()
    {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('level', $this->level, TRUE);
        $criteria->compare('code', $this->code, TRUE);
        $criteria->compare('program', $this->program, TRUE);
        $criteria->compare('course_code', $this->course_code, TRUE);
        $criteria->compare('course', $this->course, TRUE);
        $criteria->compare('type', $this->type, TRUE);
        $criteria->compare('credits', $this->credits);
        $criteria->compare('year', $this->year);
        $criteria->compare('term', $this->term, TRUE);
        $criteria->compare('description', $this->description, TRUE);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }
}
