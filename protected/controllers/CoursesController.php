<?php

class CoursesController extends Controller
{

    /**
     * @return array action filters
     */
    public function filters()
    {
        return array(
            'accessControl', // perform access control for CRUD operations
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     *
     * @return array access control rules
     */
    public function accessRules()
    {
        return array(
            array(
                'allow',
                'actions' => array(
                    'index', 'view', 'subjects', 'faculties', 'programs', 'orderedSubjects', 'autocomplete', 'docs'
                ),
                'users' => array('*'),
            ),
            array(
                'deny',
                'users' => array('*'),
            ),
        );
    }

    /**
     * Displays a particular course.
     *
     * @param integer $id the ID of the model to be displayed
     */
    public function actionView($id)
    {
        $this->render(
            'view', array(
                'model' => $this->loadModel($id),
            )
        );
    }

    public function actionSubjects()
    {
        if (isset($_GET['program']) && isset($_GET['year']) && isset($_GET['term']) && isset($_GET['level'])) {
            $program = $_GET['program'];
            $year = $_GET['year'];
            $term = $_GET['term'];
            $level = $_GET['level'];
            switch ($term) {
                case 1:
                    $term = Courses::TERM_AUTUMN;
                    break;
                case 2:
                    $term = Courses::TERM_SPRING;
                    break;
                case 3:
                    $term = Courses::TERM_SUMMER;
                    break;
                default:
                    $term = Courses::TERM_AUTUMN;
                    break;
            }
			if(!isset($_GET['type']))
				$type = array(Courses::TYPE_REGULAR, Courses::TYPE_FREE);
	        else {
	            switch ($_GET['type']) {
	                case 'норм':
	                    $type[] = Courses::TYPE_REGULAR;
	                    break;
	                case 'вибір':
	                    $type[] = Courses::TYPE_FREE;
	                    break;
	                default:
		                $type[] = Courses::TYPE_REGULAR;
	                    break;
	            }
	        }
            $cache_id = $program . $year . $term . $level.serialize($type);
            $c = Yii::app()->db->createCommand();
            $data = Yii::app()->cache->get($cache_id);
            if ($data === FALSE) {
                $model = $c->select('course, credits')->from('courses')->where(
                    array('and', 'program=:program', 'year=:year', 'term=:term', 'level=:level', array('in', 'type', $type)),
	                array(
                        ':program' => $program,
                        ':year' => $year,
                        ':term' => $term,
                        ':level' => $level
                    )
                )->queryAll();
                $data = array();
                foreach ($model as $value) {
                    $data[$value['course']] = $value['credits'];
                }
                $data = CJSON::encode($data);
                Yii::app()->cache->set($cache_id, $data);
            }
            $this->_send($data);
        } else {
           $this->_sendError(400, "Not all required parameters provided");
        }
    }

    public function actionFaculties()
    {
        if (isset($_GET['field'])) {
            $field = $_GET['field'];
            if ($field == "faculty_full" || $field == "faculty_short" || $field == "faculty_en") {
                $cache_id = $field;
                $c = Yii::app()->db->createCommand();
                $data = Yii::app()->cache->get($cache_id);
                if ($data === FALSE) {
                    $model = $c->select($field)->from('courses')->group($field)->queryAll();
                    $i = 0;
                    $data = array();
                    foreach ($model as $value) {
                        $data[$i] = $value[$field];
                        $i++;
                    }
                    $data = CJSON::encode($data);
                    Yii::app()->cache->set($cache_id, $data);
                }
                $this->_send($data);
            } else {
                $this->_sendError(400, "Incorrect value of field parameter");
            }
        } else {
            $this->_sendError(400, "Not all required parameters provided");
        }

    }

    public function actionPrograms()
    {
        if (isset($_GET['faculty']) && isset($_GET['level'])) {
            $faculty = $_GET['faculty'];
            $level = $_GET['level'];
            $cache_id = $faculty.$level;
            $data = Yii::app()->cache->get($cache_id);
            if($data===false){
                if($faculty == 'all'){
                    $programs = Yii::app()->db->createCommand()
                        ->select('program,faculty_short,faculty_en')
                        ->from('courses')
                        ->where('level=:level', array(':level'=>$level))
                        ->group('program')
                        ->order('program ASC')
                        ->queryAll();
                    foreach($programs as $program){
                        $data[$program['faculty_short']][] = $program['program'];
                    }
                }
                else{
                    $programs = Yii::app()->db->createCommand()
                        ->select('program')
                        ->from('courses')
                        ->where(array('and', 'faculty_short=:faculty', 'level=:level'), array(':faculty'=>$faculty, ':level'=>$level))
                        ->group('program')
                        ->order('program ASC')
                        ->queryAll();
                    $data = array_map(function($value){return $value['program'];}, $programs);
                }
                $data = CJSON::encode($data);
                Yii::app()->cache->set($cache_id, $data);
                }
            $this->_send($data);
        } else {
            $this->_sendError(400, "Not all required parameters provided");
        }

    }

    public function actionOrderedSubjects()
    {
        if (isset($_GET['program'])) {
            $this->orderedProgramSubjects($_GET['program']);
        } else {
            if (isset($_GET['faculty'])) {
                $this->orderedFacultySubjects($_GET['faculty']);
            } else {
                $this->_sendError(400, "Not all required parameters provided");
            }
        }
    }

    private function orderedProgramSubjects($program)
    {
        $cache_id = "ordered" . $program;
        $data = Yii::app()->cache->get($cache_id);
        if ($data === FALSE) {
            $subjects = Yii::app()->db->createCommand()->select('*')->from('courses')->where(
                array('and', 'program=:program'), array(':program' => $program)
            )->order(
                    array(
                        'field(level, "' . Courses::LEVEL_BACHELOR . '", "' . Courses::LEVEL_SPECIALIST . '", "'
                            . Courses::LEVEL_MASTER . '")', 'year',
                        'field(term, "' . Courses::TERM_AUTUMN . '", "' . Courses::TERM_SPRING . '", "'
                            . Courses::TERM_SUMMER . '")'
                    )
                )->queryAll();
            $data = CJSON::encode($subjects);
            Yii::app()->cache->set($cache_id, $data);
        }
        $this->_send($data);
    }

    private function orderedFacultySubjects($faculty)
    {
        $cache_id = "ordered" . $faculty;
        $data = Yii::app()->cache->get($cache_id);
        if ($data === FALSE) {
            $subjects = Yii::app()->db->createCommand()->select('*')->from('courses')->where(
                array('and', 'faculty_short=:faculty'), array(':faculty' => $faculty)
            )->order(
                    array(
                        'field(level, "' . Courses::LEVEL_BACHELOR . '", "' . Courses::LEVEL_SPECIALIST . '", "'
                            . Courses::LEVEL_MASTER . '")', 'year',
                        'field(term, "' . Courses::TERM_AUTUMN . '", "' . Courses::TERM_SPRING . '", "'
                            . Courses::TERM_SUMMER . '")'
                    )
                )->queryAll();
            $data = CJSON::encode($subjects);
            Yii::app()->cache->set($cache_id, $data);
        }
        $this->_send($data);
    }

    public function actionAutocomplete()
    {
        if (isset($_GET['q'])) {
            $q = str_replace(array("%", "_"), "", $_GET['q']);
            $limit = Yii::app()->request->getQuery('limit', 10);
            $offset = Yii::app()->request->getQuery('offset', 0);
            $match = Yii::app()->request->getQuery('match', 0);
            $like = ($match==0) ? "%{$q}%" : "$q";
            $data = Yii::app()->db->createCommand()->select(
                'course AS id, course, GROUP_CONCAT(DISTINCT program SEPARATOR "/") programs, GROUP_CONCAT(DISTINCT faculty_short SEPARATOR "/") faculties, term, year, level, credits'
            )->from('courses')->where('course LIKE :q', array(':q' => $like))->order('course ASC')->group('course_code')
                ->limit($limit,$offset)->queryAll();
            array_walk(
                $data, function (&$value, $key) {
                    $value['programs'] = explode("/", str_replace(",", ".", $value['programs']));
                    $value['faculties'] = explode("/", $value['faculties']);
                }
            );
            $data = CJSON::encode($data);
            $this->_send($data);
        } else {
            $this->_sendError(400, "Not all required parameters provided");
        }
    }

    private function _send($data)
    {
        $jsonp_callback = isset($_GET['callback']) ? $_GET['callback'] : NULL;
        header('Content-Type: ' . ($jsonp_callback ? 'application/javascript' : 'application/json'));
        echo $jsonp_callback ? "$jsonp_callback($data)" : $data;
        Yii::app()->end();
    }

    private function _sendError($code, $message)
    {
        header('HTTP/1.1 ' . $code . " " . $this->_statusMessage($code));
        header('Content-Type: ' . 'application/json');
        echo CJSON::encode(array("error" => $message));
        Yii::app()->end();
    }

    private function _statusMessage($code)
    {
        $codes = array(
            400 => 'Bad Request',
            404 => 'Not Found',
            500 => 'Internal Server Error', 
        );
        return (isset($codes[$code])) ? $codes[$code] : '';
    }

    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     *
     * @param integer $id the ID of the model to be loaded
     *
     * @return Courses the loaded model
     * @throws CHttpException
     */
    public function loadModel($id)
    {
        $model = Courses::model()->findByPk($id);
        if ($model === NULL) {
            throw new CHttpException(404, 'The requested page does not exist.');
        }
        return $model;
    }

}
