<?php

return array(
    'basePath' => dirname(__FILE__) . DIRECTORY_SEPARATOR . '..',
    'name' => 'USIC APIs',
    'preload' => array('log'),

    'import' => array(
        'application.models.*',
        'application.components.*',
    ),

    'components' => array(
        'db' => array(
            'connectionString' => 'mysql:host=HOST;dbname=DATABASE',
            'emulatePrepare' => TRUE,
            'username' => 'USER',
            'password' => 'PASSWORD',
            'charset' => 'utf8',
            'enableParamLogging' => TRUE,
        ),
        /*'cache' => array(
            'class' => 'application.lib.redis.CRedisCache',
            'predisPath' => 'application.lib.redis.Predis',
            'servers' => array(
                array(
                    'database' => 2,
                    'host'=>'localhost',
                    'port' => 6379,
                ),
            ),
        ),*/
        'cache' => array(
            'class' => 'CFileCache',
        ),

        'urlManager' => array(
            'urlFormat' => 'path',
            'showScriptName' => FALSE,
            'rules' => array(
                array('courses/view', 'pattern' => 'api/v1/courses/<id:\d+>', 'verb' => 'GET', 'parsingOnly' => TRUE),
                array(
                    'courses/subjects', 'pattern' => 'api/v1/courses/subjects', 'verb' => 'GET', 'parsingOnly' => TRUE
                ),
                array(
                    'courses/faculties', 'pattern' => 'api/v1/courses/faculties', 'verb' => 'GET', 'parsingOnly' => TRUE
                ),
                array(
                    'courses/programs', 'pattern' => 'api/v1/courses/programs', 'verb' => 'GET', 'parsingOnly' => TRUE
                ),
                array(
                    'courses/orderedSubjects', 'pattern' => 'api/v1/courses/orderedSubjects', 'verb' => 'GET',
                    'parsingOnly' => TRUE
                ),
                array(
                    'courses/autocomplete', 'pattern' => 'api/v1/courses/autocomplete', 'verb' => 'GET',
                    'parsingOnly' => TRUE
                )
            )
        ),
        'log' => array(
            'class' => 'CLogRouter',
            'routes' => array(
                array(
                    'class' => 'CFileLogRoute',
                    'levels' => 'error, warning',
                ),
                // uncomment the following to show log messages on web pages
                /*array(
                    'class' => 'CWebLogRoute',
                    'levels' => 'error, warning',
                ),*/
            ),
        ),

    ),
);
