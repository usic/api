#!/bin/bash

#install submodule and all dependecies
sudo apt-get update && sudo apt-get install -y php5-cli php5-common php5-mysql php5-dev git python-mysql.connector python-mysqldb mysql-server
mkdir -p {web/assets,protected/runtime,bin} && chmod 777 {web/assets,protected/runtime} -R

# INSTALL SUBMODULES
git submodule init && git submodule update

#INSTALL YII and LIBS
wget https://getcomposer.org/installer -qO - | php -- --install-dir=./bin

php ./bin/composer.phar install

echo "Please enter database credentials:"
echo DB HOST[localhost]:
read HOST
if [ -z "${HOST}" ]; then
    HOST=localhost
fi
echo DB USER[root]:
read USER
if [ -z "${USER}" ]; then
    USER=root
fi
echo DB PASSWORD[without pass]:
read PASS
if [ -z "${PASS}" ]; then
    PASS=""
fi
echo DB DATABASE[courses]:
read DB
if [ -z "${DB}" ]; then
    DB="courses"
fi
EXPORTER="./exporter/parse.py"
sed -i "s/host/$HOST/g" $EXPORTER
sed -i "s/user/$USER/g" $EXPORTER
sed -i "s/password/$PASS/g" $EXPORTER
sed -i "s/database/$DB/g" $EXPORTER

CONFIG="./protected/config/main.php"
cp protected/config/main-simple.php $CONFIG

sed -i "s/HOST/$HOST/g" $CONFIG
sed -i "s/USER/$USER/g" $CONFIG
sed -i "s/PASSWORD/$PASS/g" $CONFIG
sed -i "s/DATABASE/$DB/g" $CONFIG

mysql -u$USER -p$PASS -h$HOST -e "DROP DATABASE IF EXISTS $DB; CREATE DATABASE $DB CHARACTER SET utf8 COLLATE utf8_bin;"

chmod +x ./exporter/run.sh && cd exporter && ./run.sh
cd - > /dev/null

php -S 0.0.0.0:8080 -t web
